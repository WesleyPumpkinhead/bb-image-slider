// this file should contain the controller, directive and services
'use strict';

// app
angular.module('ZooSlider');

// controller
angular.module('ZooSlider').controller('MainCtrl', function(){

});

// directive
angular.module('ZooSlider').directive('zooSlider', function(){
	return{
		link: function(soap, el, attrs){

		}
	}
});

// services
angular.module('ZooSlider').factory('$viewportService', function(){
	var viewportService = {}
	return viewportService;

});

